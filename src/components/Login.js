import React, { Component } from 'react';
import withFirebaseAuth from 'react-auth-firebase';
import firebase from '../connect_data/firebase';


class Login extends Component {
    render() {
        const {
            signInWithGoogle,
            signInWithFacebook,
            signOut,
            user,
            error
        } = this.props;

        return (
            <div>
                {
                    user
                    ? <p>Name: {user.displayName} <br/> Email: {user.email} <br/> Photo: <img style={{width:'60px',height:'60px'}} src={user.photoURL}/></p>
                    : <div className="wraper">
                    <div className="loading">
                        <img src="assets/images/logo.png" alt />
                        <div>
                            <span/>
                            <span/>
                            <span/>
                            <span/>
                        </div>
                    </div>
                    <div className="background">
                        <div className="cloud"></div>
                        <div className="bg_black"></div>
                    </div>
                    <div className="header">
                        <img src="assets/images/logo1.png" alt />
                        <div className="text-header">
                            <h4 className="text-center">Money Lover</h4>
                        </div>
                    </div>
                    <div className="login-form">
                        <svg>
                            <rect />
                        </svg>
                        <form>
                            <div className="title text-center">
                                <span className="log">Login</span>
                            </div>
                            <div className="social">
                                <a href="#" onClick={signInWithGoogle} className="button google-plus"><span><i className="icon-google-plus" /></span>
                                    <p>Google +</p>
                                </a>
                                <a href="#" onClick={signInWithFacebook} className="button facebook"><span><i className="icon-facebook" /></span>
                                    <p>Facebook</p>
                                </a>
                            </div>
                            <div className="input-form">
                                <div className="input input-email">
                                    <input type="email" placeholder="Enter email" />
                                    <span className="bottom" />
                                </div>
                                <div className="input input-password">
                                    <input type="password" placeholder="Enter password" />
                                    <span className="bottom" />
                                </div>
                            </div>
                            <div className="boxbutton">
                                <span />
                                <span />
                                <span />
                                <span />
                                <button type="submit">Login</button>
                            </div>
                            <div className="help">
                                <a href="#">Create new account</a>
                                <span> or </span>
                                <a href="#">Forget password</a>
                            </div>
                        </form>
                    </div>
                </div>
                }
                {
                    user
                    ? <button onClick={signOut}>LogOut</button>
                    : ''
                }

            </div>
        );
    }
}

const authConfig = {
    google: {
        // redirect: true, // Opens a pop up by default
        returnAccessToken:true, // Returns an access token as googleAccessToken prop
        saveUserInDatabase: true // Saves user in database at /users ref
    },
    facebook: {
      // redirect: true, // Opens a pop up by default
      returnAccessToken: true, // Returns an access token as googleAccessToken prop
      saveUserInDatabase: true // Saves user in database at /users ref
    }
};

export default withFirebaseAuth(Login, firebase, authConfig);