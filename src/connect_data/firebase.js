import * as firebase from 'firebase';

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyCbaPTj386wa66PzDzhIZVAqmSKYSbi-EQ",
    authDomain: "moneylovermanage.firebaseapp.com",
    databaseURL: "https://moneylovermanage.firebaseio.com",
    projectId: "moneylovermanage",
    storageBucket: "",
    messagingSenderId: "354402908437",
    appId: "1:354402908437:web:df19ad5485fc54c0"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  export default firebase;